%include "lib.inc"
%include "dict.inc"
%include "colon.inc"
%include "words.inc"

%define BUFFER_SIZE 255
%define LINK_SIZE 8

section .bss
buffer: resb BUFFER_SIZE

section .rodata
start_message:
	db "Input word, please", 0
success_message:
	db "It is: ", 0
overflow_message: 
	db "The maximum string length is 255 characters", 0
not_found_message:
	db "There is no such element in the dictionary", 0
	
section .text
global _start

_start:
	mov rdi, start_message
	call print_string
	call print_newline

	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_string
	cmp rax, 0
	je .buffer_overflow

	mov rdi, rax
	mov rsi, first_word
	call find_word
	cmp rax, 0
	je .not_found

	push rax
	mov rdi, success_message
	call print_string
	pop rax

	add rax, LINK_SIZE
	mov rdi, rax
	push rdi
	call string_length
	inc rax
	pop rdi
	add rdi, rax
	call print_string
	call print_newline
	jmp exit

.buffer_overflow:
	mov rdi, overflow_message
	jmp print_error

.not_found:
	mov rdi, not_found_message
	jmp print_error
