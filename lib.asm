global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_string 

%define MINUS 0x2D
%define LINE_BREAK 0xA
%define SPACE 0x20
%define TAB 0x9
%define SYS_EXIT 60
%define STDOUT 1
%define STDERR 2
%define SYS_WRITE 1

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, SYS_EXIT
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi + rax], 0
	je .finished
	inc rax
	jmp .loop
.finished:	
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rsi
	mov rdi, STDOUT		
	mov rdx, rax		; длина строки
	mov rax, SYS_WRITE
	syscall
	ret 

print_error:
	push rdi
	call string_length
	pop rsi
	mov rdi, STDERR
	mov rdx, rax
	mov rax, SYS_WRITE
	syscall
	call print_newline
	jmp exit

; Принимает код символа и выводит его в stdout
print_char:
	push rdi			; закидываем символ в стек
	mov rax, SYS_WRITE
	mov rdi, STDOUT	
	mov rsi, rsp		; указатель меняем
	mov rdx, 1			; количество байт
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, LINE_BREAK
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r10, 10
	xor r11, r11
	mov rax, rdi
.loop:
	xor rdx, rdx
	div r10
	inc r11
	push rdx
	cmp rax, 0
	jz .print
	jmp .loop
.print:
	pop rdi
	add rdi, "0"		; перевод в ASCII код
	push r11
	call print_char
	pop r11
	dec r11
	cmp r11, 0
	jnz .print
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jnl .print
	push rdi
	mov rdi, MINUS
	call print_char
	pop rdi
	neg rdi
.print:
	jmp print_uint 

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	mov r8, rdi
	call string_length
	mov r9, rax
	mov rdi, rsi
	call string_length
	cmp rax, r9
	jne .return_false
	mov r10, 0
.compare_char:
	mov r11b, byte [rsi + r10]
	cmp byte [r8 + r10], r11b
	jne .return_false
	cmp r10, r9
	je .return_true
	inc r10
	jmp .compare_char
.return_false:
	mov rax, 0
	ret
.return_true:
	mov rax, 1
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
	xor rax, rax		; SYS_READ
	xor rdi, rdi		; STDIN
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor r8, r8
	dec rsi
	mov r9, rsi
	mov r10, rdi
	mov r11, rdi
.skip_tab:
	push r11
	call read_char
	pop r11
	cmp rax, SPACE
	je .skip_tab
	cmp rax, TAB
	je .skip_tab
	cmp rax, LINE_BREAK
	je .skip_tab
.write_buffer:
	cmp rax, 0
	je .return_true
	cmp rax, SPACE
	je .return_true
	mov byte [r10], al
	inc r10
	inc r8
	cmp r8, r9
	je .return_false
	push r11
	call read_char
	pop r11
	jmp .write_buffer
.return_false:
	xor rax, rax
	ret
.return_true:
	xor r9, r9
	mov byte [r10], r9b
	mov rax, r11
	mov rdx, r8
 	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor r9, r9
	xor r10, r10
	mov r11, 10
	push r12
	xor r12, r12
.loop:
	xor rax, rax
	mov al, byte [rdi]
	cmp rax, "0"
	jb .end_of_line
	cmp rax, "9"
	ja .end_of_line
	sub rax, "0"
	mov r10, rax
	mov rax, r9
	mul r11
	add rax, r10
	mov r9, rax
	inc rdi
	inc r12
	jmp .loop
.end_of_line:
	mov rax, r9
	mov rdx, r12
	pop r12    
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
	mov al, byte[rdi]
	cmp rax, MINUS
	je .negative
	jmp parse_uint
.negative:
	inc rdi
	call parse_uint
	neg rax
	inc rdx		
	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	ja .return_false
	mov rcx, 0
.loop:
	mov r8b, byte [rdi + rcx]
	mov byte [rsi + rcx], r8b
	cmp rcx, rax
	je .return_true
	inc rcx
	jmp .loop
.return_true:
	ret
.return_false:
	mov rax, 0
	ret  


read_string:
	xor r8, r8
	dec rsi
	mov r9, rsi
	mov r10, rdi
	mov r11, rdi
.skip_tab:
	push r11
	call read_char
	pop r11
	cmp rax, SPACE
	je .skip_tab
	cmp rax, TAB
	je .skip_tab
	cmp rax, LINE_BREAK
	je .skip_tab
.write_buffer:
	cmp rax, 0
	je .return_true
	cmp rax, LINE_BREAK
	je .return_true
	mov byte [r10], al
	inc r10
	inc r8
	cmp r8, r9
	je .return_false
	push r11
	call read_char
	pop r11
	jmp .write_buffer
.return_false:
	xor rax, rax
	ret
.return_true:
	xor r9, r9
	mov byte [r10], r9b
	mov rax, r11
	mov rdx, r8
	ret
