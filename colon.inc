;colon.inc
%define NEXT 0

%macro colon 2
%ifid %2 
%else
	%error "incorrect name for the label"
%endif

%ifstr %1
	jmp %%make_word
%else
	%error "key must be a string"
%endif

%%make_word:
%2:
	dq NEXT
	db %1, 0
%define NEXT %2
%endmacro
