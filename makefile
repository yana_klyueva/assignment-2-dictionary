ASM = nasm
ASMFLAGS = -felf64 -o
CLEANFLAGS = -rf

main: main.o lib.o dict.o
	ld -o $@ $^
#$^ - вставляет все файлы, которые на строчку выше 

%.o: %.asm
	$(ASM) $(ASMFLAGS) $@ $<

main.o: main.asm words.inc colon.inc lib.inc dict.inc
	$(ASM) $(ASMFLAGS) $@ $<

.PHONY: clean
clean: 
	rm $(CLEANFLAGS) *.o
	rm $(CLEANFLAGS) main
