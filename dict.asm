%include "lib.inc"

%define LINK_SIZE 8

global find_word

section .text
find_word:
;rdi - указатель на 0-терминированую строку, которую ищем
;rsi - указатель на 0-ой элемент словаря
.loop:
	cmp rsi, 0
	je .return_false
	push rdi
    push rsi
	add rsi, LINK_SIZE
	call string_equals
	pop rsi
	pop rdi
	cmp rax, 1
	je .return_true
	mov rsi, [rsi]
	jmp .loop
.return_true:
	mov rax, rsi
	ret
.return_false:
	xor rax, rax
	ret
